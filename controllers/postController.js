let users = require('../db/users.json');
const { user_game } = require('../models');

exports.index = (req, res) => {
  res.render('index');
};

exports.chapter3 = (req, res) => {
  res.render('chapter3');
};

exports.chapter4 = (req, res) => {
  res.render('chapter4');
};

exports.signup = (req, res) => {
  res.render('signup');
};

exports.getAll = (req, res) => {
  res.json(users);
};

exports.show = (req, res) => {
  const { id } = req.params;

  const user = users.find((i) => i.id == id);
  res.json(user);
};

exports.signUp = (req, res) => {
  const { username, password } = req.body;

  const user = {
    id: users[users.length - 1].id + 1,
    username,
    password,
  };

  users.push(user);

  res.redirect('/dashboard');
};

const readUsers = async () => {
  const users = await user_game.findAll({ raw: true });
  return users;
};

exports.dashboard = async (req, res) => {
  const users = await readUsers();
  console.log(users)
  res.render('dashboard', { users });
};

exports.add = (req, res) => {
  res.render('add');
};

exports.addUser = (req, res) => {
  const { username , password } = req.body;

  const user = {
    username,
    password,
  };

  user_game
  .create({
    username: user.username,
    password: user.password,
  })
  .then((user_game) => {
    console.log(user_game);
  });

  res.redirect('/dashboard');
}

exports.findUser = async (req, res) => {
  const { id } = req.params;

  const user = await user_game.findOne({
    where: {
      id: id
    }
  }).then((user) => {
    console.log(user)
  })

  res.render('editUser', { user });
};

exports.editUser = async(req, res) => {
  const {id} = req.params

  const user = await user_game.update({
      username: req.body.username,
      password: req.body.password,
  }, {
      where: {
          id: id
      }
  })
  res.redirect('/dashboard', { user });
}


// exports.deleteUser = async (req, res) => {
//   const id = req.params.id

//     await user_game.destroy({
//       where: {
//         id: id
//       }
//     });
//     res.redirect('/dashboard');

// }