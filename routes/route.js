const express = require('express')
const route = express.Router()
const postController = require('../controllers/postController')

// route.get('/', postController.index)
route.get('/', postController.chapter3)
// route.get('/', postController.readUser)
route.post('/', postController.signUp)
// route.get('/edit', postController.editUser)
route.get('/chapter4', postController.chapter4)
route.get('/dashboard', postController.dashboard)
route.get('/api/users', postController.getAll)
route.get('/api/users/:id', postController.show)
route.get('/add', postController.add)
route.post('/add', postController.addUser)
route.get('/edit/:id', postController.findUser)
route.put('/edit/:id', postController.editUser)
route.delete('/delete/:id', async (req, res) => {
    const id = req.params.id
    try {
        await user_game.destroy({
            where: {
              id: id
            }
          });
          res.redirect('/dashboard');
    } catch (err) {
        res.status(500).send(err);
    }
      
})

module.exports = route