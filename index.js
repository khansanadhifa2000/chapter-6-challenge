const express = require('express');
const app = express();
const port = 3000;

app.use(express.urlencoded({ extended: false }));
app.set('view engine', 'ejs');
app.use(express.static('public'));

const route = require('./routes/route');
app.use(route);

// const { user_game } = require('./models');

// app.use(express.json());

// app.get('/', async (req, res) => {
//     const users = await user_game.findAll()
//     res.json(users)
// })

//internal server error
app.use((err, req, res, next) => {
  res.status(500).json({
    status: 'fail',
    errors: err.message,
  });
});

//missing page
app.use((req, res, next) => {
  res.status(401).json({
    status: 'fail',
    errors: 'Are you lost?',
  });
});

//listen
app.listen(port, () => {
  console.log(`Web started at port : ${port}`);
});
